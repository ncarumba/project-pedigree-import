import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;



//import javax.ws.rs.core.Response;

import org.generationcp.middleware.exceptions.MiddlewareQueryException;
import org.generationcp.middleware.manager.Database;
import org.generationcp.middleware.manager.DatabaseConnectionParameters;
import org.generationcp.middleware.manager.GermplasmNameType;
import org.generationcp.middleware.manager.GetGermplasmByNameModes;
import org.generationcp.middleware.manager.ManagerFactory;
import org.generationcp.middleware.manager.Operation;
import org.generationcp.middleware.manager.api.GermplasmDataManager;
import org.generationcp.middleware.pojos.Germplasm;
import org.generationcp.middleware.pojos.GermplasmPedigreeTree;
import org.generationcp.middleware.pojos.GidNidElement;
import org.generationcp.middleware.pojos.Location;
import org.generationcp.middleware.pojos.Method;
import org.generationcp.middleware.pojos.Name;

//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.JSONValue;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;

import com.pedigreeimport.backend.Main;
import com.pedigreeimport.backend.Tokenize;




public class Snippet {
	static int gpid1;
	static int gpid2;
	static boolean male;
	static boolean female = male = true;
	 public static void main(String args[]) throws FileNotFoundException, IOException, MiddlewareQueryException  {
		 //
		 //readread();
		 //tokenize();
		 //createGID();
		 System.out.println("WELCOME!!!!");
		 DatabaseConnectionParameters local = new DatabaseConnectionParameters(
					"localhost", "3306", "local", "root", "");
			DatabaseConnectionParameters central = new DatabaseConnectionParameters(
					"localhost", "3306", "local", "root", "");
			ManagerFactory factory = new ManagerFactory(local, central);
			GermplasmDataManager manager = factory.getGermplasmDataManager();
		 //createGID(manager,"IR06H101",0,0,33);
			GermplasmPedigreeTree germplasm= new GermplasmPedigreeTree();
			germplasm=manager.getDerivativeNeighborhood(-3, 2, 1);
			System.out.println("here: "+germplasm.getRoot());
	      }
		public int createGID1(GermplasmDataManager manager, String term, int gpid1,
				int gpid2, int location) throws MiddlewareQueryException {

			int gid;
			Germplasm germplasm1 = new Germplasm();
			germplasm1.setMethodId(205);
			germplasm1.setGnpgs(0);
			germplasm1.setGpid1(gpid1);
			// int setGpid2=
			germplasm1.setGpid2(gpid2);
			germplasm1.setUserId(1);
			germplasm1.setLgid(-1);
			germplasm1.setLocationId(location);
			germplasm1.setGdate(0);
			germplasm1.setGrplce(0);
			germplasm1.setMgid(0);
			germplasm1.setReferenceId(1);
			germplasm1.setPreferredAbbreviation("N/A");
			germplasm1.setPreferredAbbreviation("N/A");

			Name name1 = new Name();
			name1.setNdate(0);
			name1.setNstat(0);
			name1.setReferenceId(0);
			name1.setUserId(0);
			name1.setLocationId(location);
			name1.setNval(term);
			name1.setTypeId(0);

			gid = manager.addGermplasm(germplasm1, name1);
			System.out.println("Germplasm" + gid);

			return gid;
		}
	 /*public static void createGID() throws  FileNotFoundException, IOException, MiddlewareQueryException {
	        DatabaseConnectionParameters local = new DatabaseConnectionParameters("localhost", "3306", "local", "root", "");
	        ManagerFactory factory = new ManagerFactory(local, null);
	        GermplasmDataManager manager = factory.getGermplasmDataManager();

	        String csv = "E:\\xampp\\htdocs\\PedigreeImport\\createdGID.csv";	//file written with created GID's


	        FileWriter fw = new FileWriter(csv, true);
	        BufferedWriter pw = new BufferedWriter(fw);
	        Object json_obj1 = JSONValue.parse(new FileReader("E:\\xampp\\htdocs\\PedigreeImport\\checked.json"));

	        JSONObject json_array1 = (JSONObject) json_obj1;
	        JSONArray obj_terms = (JSONArray) json_array1.get("checked");
	        
	        int fgid = 0, mgid = 0;

	        for (int i = 0; i < (obj_terms.size());) {

	            String csvFile = "E:\\xampp\\htdocs\\PedigreeImport\\corrected.csv";	// file to be read with the standardized germplasm names 
	            BufferedReader br = null;
	            String line = "";
	            String cvsSplitBy = ",";

	            br = new BufferedReader(new FileReader(csvFile));
	            while ((line = br.readLine()) != null) {
	                String[] column = line.split(cvsSplitBy);

	                column[3] = column[3].replaceAll("\"", "");
	                column[7] = column[7].replaceAll("\"", "");
	                column[5] = column[5].replaceAll("\"", "");
	                column[9] = column[9].replaceAll("\"", "");

	                if (column[2].equals(obj_terms.get(i))) {
	                    System.out.println("\t" + column[5] + " is " + column[3]);
	                    if (column[3].equals("in standardized format")) {
	                       
	                        tokenize(manager, pw, column[2], column[5]);
	                        fgid = gpid2;
	                    }

	                    if (column[7].equals("in standardized format")) {
	                        male = tokenize(manager, pw, column[6], column[9]);
	                        mgid = gpid2;
	                        pw.write(column[2] + "/" + column[6] + ",");
	                        pw.write(column[5] + "/" + column[9] + ",");
	                        pw.write(column[1] + ",");
	                        if (female && male) {
	                        	int gid=0;
	                            gid = (int) createGID(manager, column[1], fgid, mgid, 104);
	                            pw.write(gid + ",");	//gid
	                            pw.write("N/A" + ",");	//method ID
	                            pw.write("N/A" + ",");	//method type
	                            pw.write("N/A" + ",");	//location ID
	                            pw.write("N/A" + ",");	//location
	                            pw.write(fgid + ",");	//gpid1
	                            pw.write(mgid + ",");	//gpid2
	                        } else {
	                           pw.write("source unknown" + ",");	//gid
	                            writeFile(pw);
	                        }
	                    }
	                }
	            }
	            br.close();
	            i++;
	        }

	        //Flush the output to the file
	        pw.flush();
	        //Close the Print Writer
	        pw.close();
	        //Close the File Writer
	        fw.close();
	        //close the database connection
	        factory.close();
	        
	 }
	 */
	 
	 public static void printToFile(GermplasmDataManager manager, BufferedWriter pw, Germplasm germplasm) throws MiddlewareQueryException, IOException {
		 System.out.println("methodID: "+germplasm.getMethodId());
		 //Location location = manager.getLocationByID(germplasm.getLocationId());
		 //Method method=manager.getMethodByID(germplasm.getMethodId());
		 
		 
		 pw.write(germplasm.getGid() + ",");	//gid
		 pw.write("(" + germplasm.getMethodId() + ")" /*+ method.getMtype() + ";" + method.getMname()*/ + ",");	//method
		 pw.write("(" + germplasm.getLocationId() + ")" /*+ location.getLname()*/ + ",");	//method
		 pw.newLine();
		 
	 }
	 public static void writeFile(BufferedWriter pw) throws IOException {
	        pw.write("N/A" + ",");	//method 
	        pw.write("N/A" + ",");	//location
	        pw.newLine();
	 }
	 
	    public static boolean tokenize(GermplasmDataManager manager, BufferedWriter pw, String id, String pedigree) throws IOException, MiddlewareQueryException {
	    	 
		     //String pedigree="IR 88888-UBN 3-4";
		     String[] tokens = new Tokenize().tokenize(pedigree);
			 ArrayList<String> pedigreeList = new ArrayList<String>();
			 String s = "";
		        for (int i = 0; i <tokens.length ; i++) {
		        	if (i == 0) {
		    	    	s = s + tokens[i];
					}else{
						s = s + "-" + tokens[i];
					}
		        	pedigreeList.add(s);
		        }
		       
		       boolean prev,curr=prev=true;
		       int gid;
		       for (int i = pedigreeList.size()-1; i >= 0 ; i--) {
		    	   
		    	   pw.write(id + ",");
		    	   pw.write(pedigreeList.get(i) + ",");	//pedigree name
		    	   
		    	   int count=countGermplasmByName(manager,pedigreeList.get(i));
		    	   
		    	   System.out.print("pedigree: " + pedigreeList.get(i) +"\t");
		    	   
		    	   if(count==1){
		    		   System.out.print("\t count==1 ");
		    		   
		    		   List<Germplasm> germplasm=getGermplasmPOJOByName(pw,manager,pedigreeList.get(i),count);
		    		   
		    		   System.out.print("gid: "+germplasm.get(0).getGid());
		    		   System.out.println("\t location ID: "+germplasm.get(0).getLocationId()+ " gpid1: "+germplasm.get(0).getGpid1()+ " gpid2: "+germplasm.get(0).getGpid2());
		    		   gpid2=germplasm.get(0).getGid();
		    		   System.out.println("gpid2: "+gpid2);
		    		   
		    		   printToFile(manager,pw,germplasm.get(0));
		    		   curr=assignGID(pw, manager, pedigreeList, germplasm.get(0).getGpid1(),germplasm.get(0).getLocationId(), i, id);	// assign GID's to the pedigree line 
		    		   break;
		    		   
		    	   }else if(count>1 || !prev){
		    		   System.out.print("\t count>1 ");
		    		   curr=false;	// false,  a flag to catch the pedigree with existing GID's that sets the pedigree line  
		    		   System.out.println("choose GID");
		    		   
		    		   getGermplasmByName(manager,pedigreeList.get(i),count);
		    		   
		    	   }else if(count==0){
		    		   System.out.print("\t count==0 ");
		    		   if(i>0){
			    		   System.out.println("NOT SET");
			    		   
			    		   pw.write("NOT SET"+",");
			    		   writeFile(pw);
			    		   curr=false;
		    		   }else{
		    			   System.out.println("create GID");
		    			   assignGID_fromRoot(pw, manager, pedigreeList, 33, id);
		    			   curr=true;
		    		   }
		    		   
		    	   }
		    	   prev=curr;
		    	   
		        }
		       return prev;
	    }
	    
	    public static void assignGID_fromRoot(BufferedWriter pw, GermplasmDataManager manager, ArrayList<String> pedigreeList, int locationID, String id) throws MiddlewareQueryException, IOException{
	    	int gpid2 = 0,gpid1=0, gid;
	    	ArrayList<Integer> pedigreeList_GID= new ArrayList<Integer>(); 
	    	
	    	for (int i = 0; i <pedigreeList.size() ; i++) {
	    		
		    	   
	    		gid = (int) createGID(manager, pedigreeList.get(i), gpid1, gpid2, locationID);
	    		
	    		if(i==0){
	    			gpid2=gid;
	    			gpid1=gid;
	    		}else{
	    			gpid2=gid;
	    		}
	    		pedigreeList_GID.add(gid);
	    		System.out.println(pedigreeList.get(i)+" gpid1: " + gpid1+" gpid2: " + gpid2);
	        }
	    	
	    	for (int i = pedigreeList_GID.size()-1; i >= 0 ; i--) {
	    		
	    		Germplasm germplasm=manager.getGermplasmByGID(pedigreeList_GID.get(i));
	    		System.out.print("pedigree: "+ pedigreeList.get(i));
	    		System.out.println("\t gid: "+germplasm.getGid()+ " gpid1: "+ germplasm.getGpid1()+" gpid2: "+ germplasm.getGpid2());
	    		
	    		pw.write(id + ",");
		    	pw.write(pedigreeList.get(i) + ",");	//pedigree name
	    		printToFile(manager,pw,germplasm);
	    	}
	    	
	    }
	    
	    public static boolean assignGID(BufferedWriter pw, GermplasmDataManager manager, ArrayList<String> pedigreeList, int gpid1,int locationID, int index, String id) throws MiddlewareQueryException, IOException{
	    	
	    	Boolean flag=false;
	    	
	    	//Assign GID from ith index, forward search from root to most recent pedigree
	    	for (int i = index-1; i >=0 ; i--) {
	    		int count=countGermplasmByName(manager,pedigreeList.get(i));
	    		System.out.println("\npedigree: "+pedigreeList.get(i));
	    		System.out.println("number of rows: " +count);
	    		getGermplasmByName(manager, pedigreeList.get(i), count);
	    		
	    		pw.write(id + ",");
		    	pw.write(pedigreeList.get(i) + ",");	//pedigree name
	    		
	    		if(count>0){
	        		List<Germplasm> germplasm=getGermplasmPOJOByName(pw, manager,pedigreeList.get(i),count);
	    			Germplasm germplasm1=getGermplasm_among_existing(manager, germplasm, pedigreeList.get(i).toString(), gpid1, locationID);
	    			
	    			if (germplasm1.getGid() == null) {
	    				System.out.println("\t "+pedigreeList.get(i)+ " is unknown");
	    				
	    				pw.write("NOT SET"+",");
			    		writeFile(pw);
	    				flag=false;
	    			    
	    			}else{
	    				//pedigreeList_GID.add(germplasm1.getGid());
	    				System.out.print("\t gpid1: " + germplasm1.getGpid1());
	    				System.out.print("\t gpid2: " + germplasm1.getGpid2());
	    				System.out.println("\t location: " + germplasm1.getLocationId());
	    				printToFile(manager,pw,germplasm1);
	    				flag=true;
	    			}
	    		}else{
	    			System.out.println("\t"+pedigreeList.get(i)+ " is unknown");
	    			
	    			pw.write("NOT SET"+",");
		    		writeFile(pw);
	    			flag=false;
	    		}
	        }
	    	
	    	//Assign GID from ith index, backtrack from most recent pedigree to root
	    	for (int i = index+1; i <pedigreeList.size() ; i++) {
	    		System.out.print("\npedigree: "+pedigreeList.get(i));
	    		int count=countGermplasmByName(manager,pedigreeList.get(i));
	    		
	    		pw.write(id + ",");
		    	pw.write(pedigreeList.get(i) + ",");	//pedigree name
	    		if(count>0){
	        		List<Germplasm> germplasm=getGermplasmPOJOByName(pw, manager,pedigreeList.get(i),count);
	    			Germplasm germplasm1=getGermplasm_among_existing(manager, germplasm, pedigreeList.get(i).toString(), gpid1, locationID);
	    			System.out.println("from pedigree: "+pedigreeList.get(i)+" gpid1: " + germplasm1.getGpid1()+" gpid2: " + germplasm1.getGpid2()+" location: "+ germplasm.get(i).getLocationId());
	    			flag=true;
	    		}else{
	    			//create GID for the pedigree with gpid1='gpid1'
	    			System.out.println("create GID for "+pedigreeList.get(i));
	    			int gid = (int) createGID(manager, pedigreeList.get(i), gpid1, gpid2, locationID);
	    			gpid2=gid;
	    			Germplasm germplasm1=manager.getGermplasmByGID(gid);
	    			System.out.print("\t gpid1: " + germplasm1.getGpid1());
    				System.out.print("\t gpid2: " + germplasm1.getGpid2());
    				System.out.println("\t location: " + germplasm1.getLocationId());
    				
    				printToFile(manager,pw,germplasm1);
    				flag=true;
	    		}
	        }
	    	return flag;
	    }
	    
	    public static Germplasm getGermplasm_among_existing(GermplasmDataManager manager, List<Germplasm> germplasm,String pedigree, int gpid1,int locationID) throws MiddlewareQueryException{
	    	Germplasm germplasm1 = new Germplasm();
	    	for(int i=0; i < germplasm.size(); i++){
	    		/*	finding the germplasm with same gpid1(root GID) as the proceeding pedigree's
	    		 * 	gpid1 or same gpid1 to the root's gid, and same locationID as the 
	    		 * 	preceeding's location ID 
	    		*/
	    		
	    		if((germplasm.get(i).getGpid1().equals(gpid1) || germplasm.get(i).getGid().equals(gpid1)) &&  germplasm.get(i).getLocationId().equals(locationID)){
	    			System.out.print("chosen germplasm: " +germplasm.get(i).getGid());
					return germplasm1=germplasm.get(i);
	    		}
	    	}
    			return germplasm1;
	    }
	    
	    public static List<Germplasm> getGermplasmPOJOByName(BufferedWriter pw, GermplasmDataManager manager,
	    		String pedigree, int count) throws MiddlewareQueryException, IOException {

	    	List<Germplasm> germplasm= new ArrayList<Germplasm>();
			germplasm=manager.getGermplasmByName(pedigree, 0, count, GetGermplasmByNameModes.NORMAL, Operation.EQUAL, 0, null, Database.LOCAL);
			return germplasm;
	    }


	    public static int countGermplasmByName(GermplasmDataManager manager, String s) throws MiddlewareQueryException {

	        int count = (int) manager.countGermplasmByName(s, GetGermplasmByNameModes.NORMAL, Operation.EQUAL, 0, null, Database.LOCAL);
	        return count;
	    }

	    public static void getGermplasmByName(GermplasmDataManager manager, String s, int count) throws MiddlewareQueryException, IOException {
	    	/*File file = new File("E:\\xampp\\htdocs\\PedigreeImport\\existingTerm.csv");
	    	 
			if(file.delete()){
				System.out.println(file.getName() + " is deleted!");
			}else{
				System.out.println("Delete operation is failed.");
			}
			*/
	    	
	        String csv = "E:\\xampp\\htdocs\\PedigreeImport\\existingTerm.csv";	//file written with existing GID

	        FileWriter fw = new FileWriter(csv, true);
	        BufferedWriter pw = new BufferedWriter(fw);

	        List<Germplasm> germplasm = new ArrayList<Germplasm>();
	        germplasm = manager.getGermplasmByName(s, 0, count, GetGermplasmByNameModes.NORMAL, Operation.EQUAL, 0, null, Database.LOCAL);
	        //System.out.println("GID: "+germplasm.toString());
	        //System.out.println("size: " + germplasm.size());

	        String location = "N/A";
	        String methodType = "N/A";
	        for (int i = 0; i < germplasm.size(); i++) {
	            //System.out.println(germplasm.get(i).getGid());
	            System.out.print(s + ",");
	            System.out.print(germplasm.get(i).getGid() + ",");
	            System.out.print(germplasm.get(i).getMethodId() + ",");
	            System.out.print(methodType + ",");
	            System.out.print(" location: "+germplasm.get(i).getLocationId() + ",");
	            System.out.print(location + ",");
	            System.out.print(germplasm.get(i).getGpid1() + ",");
	            System.out.println(germplasm.get(i).getGpid2() + ",");
	            pw.newLine();
	        }

	        pw.flush();
	        pw.close();
	        fw.close();
	    }
	    

	    public static int createGID(GermplasmDataManager manager, String term, int gpid1, int gpid2, int location) throws MiddlewareQueryException {

	        int gid;
	        Germplasm germplasm1 = new Germplasm();
	        germplasm1.setMethodId(105);
	        germplasm1.setGnpgs(0);
	        germplasm1.setGpid1(gpid1);
	        //int setGpid2=
	        germplasm1.setGpid2(gpid2);
	        germplasm1.setUserId(1);
	        germplasm1.setLgid(-1);
	        germplasm1.setLocationId(location);
	        germplasm1.setGdate(0);
	        germplasm1.setGrplce(0);
	        germplasm1.setMgid(0);
	        germplasm1.setReferenceId(1);
	        germplasm1.setPreferredAbbreviation("N/A");
	        germplasm1.setPreferredAbbreviation("N/A");

	        Name name1 = new Name();
	        name1.setNdate(0);
	        name1.setNstat(0);
	        name1.setReferenceId(0);
	        name1.setUserId(0);
	        name1.setLocationId(location);
	        name1.setNval(term);
	        name1.setTypeId(0);

	        gid = manager.addGermplasm(germplasm1, name1);
	        System.out.println("Germplasm" + gid);

	        return gid;
	    }
	    
}