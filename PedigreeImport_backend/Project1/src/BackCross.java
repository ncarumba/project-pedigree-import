
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author ncarumba
 */
public class BackCross {

    static char currChar, prevChar;
    ArrayList<String> storeSlash = new ArrayList<String>();

    /**
     *
     * @param aline
     */
    public void main(String aline) {
        BackCross bc = new BackCross();
        //String line = "A//B/C///A/B//C";
        String line = aline;
        //System.out.println(line);

        int max = maxCross(line);
        method(line, max);
    }

    private void method(String line, int max) {
        String temp = line;

        Pattern p1 = Pattern.compile("\\*\\d");
        Matcher m = p1.matcher(line);

        while (m.find()) {
            String[] tokens = temp.split("\\*\\d", 2);
            print(tokens);

            String slash = "";
            max++;
            for (int j = max; j > 0;) {
                slash = slash + "/";
                j--;
            }
            System.out.println("token: " + tokens[0]);
            tokens[0] = tokens[0].concat(slash).concat(tokens[0]);
            temp.replaceFirst("\\*\\d", tokens[0]);
            temp = tokens[0].concat(tokens[1]);
            System.out.println("token: " + temp);
            System.out.println("end of while ");
        }
    }

    void print(String[] tokens) {
        System.out.println(Arrays.toString(tokens));
    }

    private static int maxCross(String line) {
        int count = 0, start = 0, end = line.length(), max = 0;
        while (start < end) {
            currChar = line.charAt(start);
//                System.out.print(currChar);
            if (currChar == '/') {
                count++;
                if (max < count) {
                    max = count;
                }
            } else {
                count = 0;
            }
            start++;
        }
        //System.out.println("max: " + max);
        return max;
    }
}
