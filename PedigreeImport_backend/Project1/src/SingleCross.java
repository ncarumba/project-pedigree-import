
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author ncarumba
 */
public class SingleCross {

    static char currChar, prevChar;
    List<List<String>> twoDim = new ArrayList<List<String>>();
    List<String> row = new ArrayList<String>();

    public void main(String aline) {
        //String line = "A//B/C///A/B//C";
        String line = aline;        
        String temp = line;

        int familyCount = 0;
        int max = maxCross(line);

        //System.out.println(">>" + max + " family/s identified");
        row.add(temp);
        row.add("n");
        twoDim.add(row);
        //System.out.println("list:" + twoDim);
        method(max, familyCount);
        //System.out.println("familyCount:" + familyCount);
    }

    private int method(int max, int familyCount) {


        if (max > 0) {
            String slash = "";
            for (int i = max; i > 0;) {
                slash = slash + "/";
                i--;
            }

            for (int i = 0; i < twoDim.size(); i++) {
                for (int j = 0; j < row.size(); j++) {
                    if ("n".equals(twoDim.get(i).get(1))) {
                        //System.out.println("token: " + twoDim.get(i).get(0));

                        Pattern p1 = Pattern.compile(slash);
                        Matcher m = p1.matcher(twoDim.get(i).get(0));

                        while (m.find()) {
                            String[] temp2 = twoDim.get(i).get(0).split(slash + "|\\+");   // ncarumba used the character + just to flag where to split the string
                            //System.out.println(Arrays.toString(temp2));
                            familyCount++;
                            for (int k = 0; k < temp2.length; k++) {
                                row = new ArrayList<String>();
                                row.add(temp2[k]);
                                row.add("n");
                                twoDim.add(row);

                                if (k % 2 == 0) {
                                    System.out.println("\n(family" + familyCount + ") female:   " + temp2[k]);
                                } else {
                                    System.out.println("(family" + familyCount + ") male:     " + temp2[k]+"\n");
                                }
                                if(!temp2[k].contains("/")){
                                    //new IRRI().standardIRRI(temp2[k]);
                                }
                            }
                            twoDim.get(i).remove(1);
                            twoDim.get(i).add("y");
                            //System.out.println("end finding "+slash);
                        }

                        //System.out.println("list:" + twoDim);
                    }

                }
            }
        } else {
            System.exit(0);
        }
        //System.out.println();
        method(max - 1, familyCount);
        return familyCount;

    }

    private static int maxCross(String line) {
        int count = 0, start = 0, end = line.length(), max = 0;
        while (start < end) {
            currChar = line.charAt(start);
            if (currChar == '/') {
                count++;
                if (max < count) {
                    max = count;
                }
            } else {
                count = 0;
            }
            start++;
        }
        return max;
    }
}  // end class SingleCross
