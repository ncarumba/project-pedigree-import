
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author NCarumba
 */
public class Main {

    private static String line;

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Main m = new Main();
        m.inputString();
        m.checkString();
    }

    private void inputString() {    // String input
        Scanner user_input = new Scanner(System.in);

        System.out.print("Enter String:");
        line = user_input.nextLine();
    }

    public void checkString() {
        if (line.contains("/")) {   //checks it is BackCross or Single Cross
            if (line.contains("*")) {       //if it is BackCross
                new BackCross().main(line);
            } else {    //if it is Single Cross
                new SingleCross().main(line);
            }
        } else {
            Pattern p = Pattern.compile("IR");
            Matcher m = p.matcher(line);

            if (m.lookingAt()) {    // Breeding Line is IRRI
                new IRRI().standardIRRI(line);
            }
            Pattern p1 = Pattern.compile("WA");
            Matcher m1 = p1.matcher(line);

            if (m1.lookingAt()) {   // Breeding line is WARDA
                System.out.println("WARDA line");
                new WARDA().standardWARDA(line);

            }
        }
    }
}   // end class Main
