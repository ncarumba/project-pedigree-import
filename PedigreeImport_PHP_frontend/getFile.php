<?php
include("top.txt");
include("methods.txt");

if(isset($_FILES['file']['name'])){
    $tmpName = $_FILES['file']['tmp_name'];
    $newName =getcwd(). UPLOADEDFILES . $_FILES['file']['name'];
    if(!is_uploaded_file($tmpName) || !move_uploaded_file($tmpName, $newName)){
        echo "FAILED TO UPLOAD " . $_FILES['file']['name'] .
                 "<br>Temporary Name: $tmpName <br>";
		} else {
			save_document_info_json($_FILES['file']);
		}
   } else {
     echo "You need to select a file.  Please try again.";
  }
  

?>
<script type="text/javascript">
		function Check(chkOptions, chkState)
		{
			for (i=0; i<chkOptions.length; i++)
			{
				chkOptions[i].checked = chkState;
			}
		return;
		}
	</script>
<outercode>

<form action="getFile.php" method="post"enctype="multipart/form-data">
	<table width="100%";>
		<tr>
			<td width="50%">
				<fieldset>
					<legend style="color:rgb(71, 134, 124);font-weight:bold;">Select List Type</legend>
					<input type="radio" name="group1" value="Breeders Cross Histories"> Breeders Cross Histories<br>
					<input type="radio" name="group1" value="Cultivar List" checked> Cultivar List<br>
					<input type="radio" name="group1" value="Accession"> Accession
				</fieldset><br>
			
				<label for="file">Browse:</label><br>
				<input type="file" name="file" id="file">
				<input type="submit" name="submit" value="Upload List"><br><br>
				
				Location<br><br>
				<select name="location" style="width:100%">
					<option value="location1">location 1</option>
					<option value="location2">location2</option>
					<option value="location3" selected="selected">IRRI HQ</option>
					<option value="location4">location4</option>
				</select>
			</td>
			<td width="50%">
				<fieldset>
					<legend style="color:rgb(71, 134, 124);font-weight:bold;">Map excel Columns to Table Columns</legend>
					<table width="100%">	
						<tr>
							<td width="33.33%">
								Excel Columns<br>
								<textarea name="excel_col" rows="9" cols="30"></textarea>
							</td>
							<td width="33.33%">
								<center>
								<button name="map" type="button">Map</button><br>
								<button name="mapAs" type="button">Map as...</button><br>
								<button name="umap" type="button">Unmap</button><br><br><br>
								<button name="processAll" type="button">Process All</button><br>
								</center>
							</td>
							<td width="33.33%">
								Table Columns<br>
								<textarea name="table_col" rows="9" cols="30"></textarea>
							</td>
						</tr>
					</table>
				
				</fieldset>

			</td>
		</tr>
	</table>
</form>
</outercode>

<outercode>

	<fieldset>
			<legend style="color:rgb(71, 134, 124);font-weight:bold;">Germplasm List</legend>
			<?php
				$fp   = fopen("corrected.csv","r"); 
				$rows = array(); 
				while (($row = fgetcsv($fp)) !== FALSE) { 
					$rows[] = $row; 
				} 
				fclose($fp); 
			?>
			<?php
			echo '<table cellpadding="0" cellspacing="1" border="0" style="width:100%" class="tableborder">';
				echo "<thead>
						<tr>
						<th> </th>
						<th>GID</th>
						<th>Female Parent</th>
						<th>Male Parent</th>";
						echo'<form action="fixedString.php" method="post">
						<th>Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>';
						?><th>New GID</th>
					</tr>
				</thead>
				<tbody>
				<?php/* $i=0;
				foreach ($rows as $row) : list($GID,$female,$fid,$fremarks,$fgid,$male,$mid,$mremarks,$mgid) = $row; 
				$termArray[$i] = $female;
				$termArray[$i+1] = $male;
				$i+=2;
				endforeach;
				print_r($termArray);*/
				?>
					<?php $termArray=array(); $i=0; 
							foreach ($rows as $row) : list($GID,$fid,$fremarks,$fgid,$female,$mid,$mremarks,$mgid,$male) = $row; 
						echo '<tr> ';
							echo '<td></td>';
							echo '<td>'.$GID.'</td> ';
							if(strcmp($fremarks, 'in standardized format')==0){
								echo '<td title='.$fremarks.'>'.$female.'</td>'; 
							}
							else{
								echo '<td title='.$fremarks.'><kbd>'.$female.'</kbd></td>'; 
							}
							if(strcmp($mremarks, 'in standardized format')==0){
								echo '<td title='.$mremarks.'>'.$male.'</td>'; 
							}
							else{
								echo '<td title='.$mremarks.'><kbd>'.$male.'</kbd></td>'; 
							}
							echo '<td>';?>

								<table>
									<tr>
										<?php
										//$id=$fid;
										echo "<td title='$fremarks'>";
										if(strcmp($fremarks, 'in standardized format')==0){
											echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Female Parent is in standardized format<br>";
										}else{
											echo 'Female Parent is <kbd>NOT</kbd> in standardized format<br>';
											
											//$postvalue=array("a","b","c");ar);
											
											//array_push($termArray,$female);
											//print_r($termArray);
											//	echo '<input type="hidden" name="termArray" value='.$female.'>';		
										}?>
										</td>
									</tr>
									<tr>
										<?php
										//$id=$mid;
										echo "<td title='$mremarks'>";
										if(strcmp($mremarks, 'in standardized format')==0){
											echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Male Parent is in standardized format<br>";
										}else{
											echo 'Male Parent is <kbd>NOT</kbd> in standardized format';
											
											
											//array_push($termArray,$male);
											//print_r($termArray);
										}?>
										</td>
									</tr>
								</table>
							</td>
							<td>
								<pre><?php echo $fgid; ?></pre> 
								<pre><?php echo $mgid; ?></pre>
							</td>
						</tr> 
					<?php  endforeach; ?> 
				</tbody> 
			</table>
<input type="submit" value="submit"/>	</form>		
	</fieldset>
</outercode>

<outercode>
	<table width="100%">
		<tr>
			<td>
			<fieldset>
				<legend style="color:rgb(71, 134, 124);font-weight:bold;">Tree</legend>
				<textarea name="table_col" rows="10" cols="70"></textarea>
			</fieldset>
			</td>
			<td>
			<table width="100%">
				<tr>
					<td>
						<fieldset>
							<legend style="color:rgb(71, 134, 124);font-weight:bold;">Details</legend>
							<textarea name="table_col" rows="4" cols="70"></textarea>
						</fieldset>
					</td>
				</tr><tr>
					<td>
						<fieldset>
							<legend style="color:rgb(71, 134, 124);font-weight:bold;">Events Log</legend>
							<textarea name="table_col" rows="4" cols="70"></textarea>
						</fieldset>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</outercode>


</div>
<!-- END CONTENT -->

<?php
include("bottom.txt");
?>

