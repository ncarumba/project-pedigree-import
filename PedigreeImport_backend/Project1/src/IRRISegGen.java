
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author ncarumba
 */
public class IRRISegGen {

    private static String A_IR = "(IR)"; // IRRI Line
    private static String A_SPACE = "(\\s)";   // white space
    private static String A_PLANT_NO = "(\\d+)";  // plant number
    private static String A_LOC = "(UBN|AJY|SRN|CPA|KKN|PMI|SKN|SRN)";   //location
    private static String A_SEL_NO = "(\\d+)"; //selection number
    private static String A_DASH = "(-)";  //dash
    private static String A_MP = "(\\d+MP)";   //mapping population followed by the plant number
    private static String A_BM = "((\\d{0,4}B)|R|AC|(C\\d+))"; // breeding methods: “B” - bulk, “R” - rapid generation advance or single seed descent, “AC” - anther culture. “C” - for composite populations; a succeeding number indicates the specific cycle of composite
    private String line;
    private String tokens[];
    private ArrayList<String> toFix = new ArrayList<String>();
    private Pattern p = Pattern.compile(A_IR + A_PLANT_NO); // no space between IR and plant number
    private Pattern p1 = Pattern.compile("(^\\s+)(.)"); //space/s after dash
    private Pattern p2 = Pattern.compile(A_LOC + A_SEL_NO); // no space between the location number and the selection number
    private Pattern p3 = Pattern.compile("(\\d{0,5})(\\s+)(B)"); //space/s between the bulk number and the bulk code
    private Pattern p4 = Pattern.compile("(\\d+)(\\s+)(MP)");    ///space/s between the mapping population and the plant number
    private Pattern p5 = Pattern.compile("(C)(\\s+)(\\d+)"); //space/s between the composite population code and the plant number
    private Pattern p6 = Pattern.compile("(.)(\\s+)($)");    //space/s at the end of the string or before dash
    Tokenize t = new Tokenize();

    /**
     *
     * @param aline
     */
    public void checkErrors(String aline) {

        line = aline;
        tokens = t.tokenize(line);
        String temp = line;

        for (int i = 0; i < tokens.length; i++) {
            //toFix.add(new ArrayList<String>());
            checkErrorSpacing(i, temp);
            checkErrorPattern(i, temp);
        }
        fixString();
        correctFixedLine();
        //System.out.println(toFix);
    }

    private void checkErrorSpacing(int i, String temp) {
        // Error Trapping: no space between IR and plant number eg. IR^88888
        Matcher m = p.matcher(tokens[i]);
        if (m.matches()) {
            //System.out.println(m);
            temp = temp.replaceAll(m.group(1), m.group(1) + "^");
            System.out.println(temp + "\t;space expected between IR and plant number");
            //fixString(m, "add_space");
            if (!toFix.contains("m")) {
                toFix.add("m");    // add m (string for Matcher m)
            }
        }
        //Error Trapping: space is found at the beginning of the string
        Matcher m1 = p1.matcher(tokens[i]);
        if (m1.find()) {
            //printGroup(m1);
            temp = line;
            //System.out.println("m: " + m7);
            temp = temp.replaceAll(m1.group(0), m1.group(1) + "^" + m1.group(2));
            System.out.println(temp + "\t;unexpected space is found at the beginning of the tokrn");
            // fixString(m1, "ws_at_start");
            if (!toFix.contains("m1")) {
                toFix.add("m1");    // add m1 (string check for Matcher m1)
            }
        }
        // Error Trapping: no space between location code and plant number  eg. KKN^7879
        Matcher m2 = p2.matcher(tokens[i]);

        if (m2.matches()) {
            //printGroup(m2);
            temp = line;
            temp = temp.replaceAll(m2.group(1), m2.group(1) + "^");
            System.out.println(temp + "\t;space expected between location code and plant number");
            //fixString(m2, "add_space");
            if (!toFix.contains("m2")) {
                toFix.add("m2");    // add m2 (string check for Matcher m2)
            }
        }
        // Error Trapping: space is found between number of bulks and the bulk code eg.4 ^B
        Matcher m3 = p3.matcher(tokens[i]);
        if (m3.matches()) {
            //printGroup(m3);
            temp = line;
            temp = temp.replaceAll(m3.group(0), m3.group(1) + m3.group(2) + "^" + m3.group(3));
            System.out.println(temp + "\t;unexpected space is found between number of bulks and the bulk code");
            //fixString(m3, "remove_space");
            if (!toFix.contains("m3")) {
                toFix.add("m3");    // add m3 (string check for Matcher m3)
            }
        }
        // Error Trapping: space is found between plant number and th mapping population code eg. 4 ^MP
        Matcher m4 = p4.matcher(tokens[i]);
        if (m4.matches()) {
            //printGroup(m4);
            temp = line;
            temp = temp.replaceAll(m4.group(0), m4.group(1) + m4.group(2) + "^" + m4.group(3));
            System.out.println(temp + "\t;unexpected space is found between plant number and th mapping population code");
            //fixString(m4, "remove_space");
            if (!toFix.contains("m4")) {
                toFix.add("m4");    // add m4 (string check for Matcher m4)
            }
        }
        // Error Trapping: space is found between plant number and th mapping population code eg. C ^88
        Matcher m5 = p5.matcher(tokens[i]);
        if (m5.matches()) {
            //printGroup(m5);
            temp = line;
            temp = temp.replaceAll(m5.group(0), m5.group(1) + m5.group(2) + "^" + m5.group(3));
            System.out.println(temp + "\t;unexpected space is found between plant number and the composite population code");
            //fixString(m5, "remove_space");
            if (!toFix.contains("m5")) {
                toFix.add("m5");    // add m5 (string check for Matcher m5)
            }
        }
        //Error Trapping: space is found at the end of the string
        Matcher m6 = p6.matcher(tokens[i]);
        if (m6.find()) {
            //printGroup(m6);
            temp = line;
            //System.out.println("m: " + m5);
            temp = temp.replaceAll(m6.group(0), m6.group(1) + m6.group(2) + "^");
            System.out.println(temp + "\t;unexpected space is found at the end of the token");
            //fixString(m6, "ws_at_end");
            if (!toFix.contains("m6")) {
                toFix.add("m6");    // add m6 (string check for Matcher m6)
            }
        }
    }

    /**
     *
     * @param i the ith token of the string
     * @param temp a temporary variable to hold the string line
     */
    public void checkErrorPattern(int i, String temp) {   //ERROR TRAPPING: pattern/s not recognized, unrecognized codes
        Pattern p11 = Pattern.compile("(\\d+)|(IR\\s\\d+)|((UBN|AJY|SRN|CPA|KKN|PMI|SKN|SRN)\\s\\d+)|(\\d{0,4}B)|R|AC|(C\\d+)|(\\d+MP)");
        Matcher m11 = p11.matcher(tokens[i]);
        if (!m11.matches()) {
            temp = temp.replaceAll(tokens[i], tokens[i] + "^");
            System.out.println(temp + "\t;string pattern not recognized ");
        }
    }

    public void checkErrorPattern() {   //ERROR TRAPPING: pattern/s not recognized, unrecognized codes
        for (int i = 0; i < tokens.length; i++) {
            String temp = line;
            Pattern p11 = Pattern.compile("(\\d+)|(IR\\s\\d+)|((UBN|AJY|SRN|CPA|KKN|PMI|SKN|SRN)\\s\\d+)|(\\d{0,4}B)|R|AC|(C\\d+)|(\\d+MP)");
            Matcher m11 = p11.matcher(tokens[i]);
            if (!m11.matches()) {
                temp = temp.replaceAll(tokens[i], tokens[i] + "^");
                System.out.println(temp + "\t;string pattern not recognized ");
            }
        }
    }

    private void correctFixedLine() {
        Pattern p12 = Pattern.compile(A_IR + A_SPACE + A_PLANT_NO + "(" + A_DASH + "(((" + A_LOC + A_SPACE + A_SEL_NO + ")|" + A_SEL_NO + ")|" + A_BM + "|" + A_MP + ")){1,5}");
        //Pattern p2 = Pattern.compile("IR\\s\\d+(-((((UBN|AJY|SRN|CPA|KKN|PMI|SKN|SRN)\\s\\d+)|\\d+)|((\\d{0,4}B)|R|AC|(C\\d+))|(\\d+MP))){1,5}");
        Matcher m = p12.matcher(line);
        if (m.matches()) {
            //System.out.println(" correct");
            tokens = t.tokenize(line);
            t.stringTokens(tokens);
        }
    }

    private void fixString() {
        Matcher m = null;
        String answer;
        do {
            Scanner user_input = new Scanner(System.in);
            System.out.print("\n>>Fix String? (Y/N) ");
            answer = user_input.nextLine();

            if (answer.equalsIgnoreCase("Y")) {
                for (int i = 0; i < tokens.length; i++) {
                    if (toFix.contains("m2")) {
                        m = p2.matcher(tokens[i]);
                        if (m.matches()) {
                            line = line.replaceAll(m.group(1), m.group(1) + " ");
                            //System.out.println("processed.");
                        }
                    }
                    if (toFix.contains("m")) {
                        m = p.matcher(tokens[i]);
                        if (m.matches()) {
                            line = line.replaceAll(m.group(1), m.group(1) + " ");
                            //System.out.println("processed.");
                        }
                    }

                    if (toFix.contains("m3")) {
                        m = p3.matcher(tokens[i]);
                        if (m.matches()) {
                            line = line.replaceAll(m.group(1) + m.group(2) + m.group(3), m.group(1) + m.group(3));
                            //System.out.println("processed.");
                        }
                    }
                    if (toFix.contains("m4")) {
                        m = p4.matcher(tokens[i]);
                        if (m.matches()) {
                            line = line.replaceAll(m.group(1) + m.group(2) + m.group(3), m.group(1) + m.group(3));
                            //System.out.println("processed.");
                        }
                    }
                    if (toFix.contains("m5")) {
                        m = p5.matcher(tokens[i]);
                        if (m.matches()) {
                            line = line.replaceAll(m.group(1) + m.group(2) + m.group(3), m.group(1) + m.group(3));
                            //System.out.println("processed.");
                        }
                    }
                    if (toFix.contains("m1")) {
                        m = p1.matcher(tokens[i]);
                        if (m.find()) {
                            line = line.replaceAll(m.group(0), m.group(2));
                            //System.out.println("processed.");
                        }
                    }
                    if (toFix.contains("m6")) {
                        m = p6.matcher(tokens[i]);
                        if (m.find()) {
                            line = line.replaceAll(m.group(1) + m.group(2), m.group(1));
                            //System.out.println("processed.");
                        }
                        //System.out.println("string: " + line + "\n");
                    }
                }
                System.out.println("processed.");
                tokens = t.tokenize(line);
                checkErrorPattern();

            } else if (answer.equalsIgnoreCase("N")) {
                System.exit(1);
            }
        } while (answer.equalsIgnoreCase("Y") == false);
    }

    private static void printGroup(Matcher m) {
        System.out.println("Group count: " + m.groupCount());
        int i;
        for (i = 0; i <= m.groupCount(); i++) {
            System.out.println(i + " : " + m.group(i));
        }
    }

    private void printTokens() {
        System.out.println();
        for (int i = 0; i < tokens.length; i++) {
            System.out.println(tokens[i]);
        }
    }
}
