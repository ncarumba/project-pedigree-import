
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author ncarumba
 */
public class WARDA {

    private static String A_WA = "WA"; // IRRI Line
    private static String A_STATION = "(B|S|R|T)"; // cross number
    private static String A_CN = "(\\d+)"; // cross number
    private String line;
    Tokenize t = new Tokenize();
    private String tokens[];

    String standardWARDA(String aline) {
        line = aline;
        Pattern p = Pattern.compile(A_WA + A_STATION + "(\\d+" + "((-" + A_CN + "){0,}))?");
        Matcher m = p.matcher(line);
        if (m.matches()) {
            System.out.println(line);
            System.out.println(" correct");
            tokens = t.tokenize(line);
            t.stringTokens(tokens);
        } else {
            System.out.println("\n>>String not properly formatted.. ");
            checkErrors(line);
        }
        return line;
    }

    private void checkErrors(String line) {
        tokens = t.tokenize(line);
        String temp = line;

        for (int i = 0; i < tokens.length; i++) {
            //toFix.add(new ArrayList<String>());
            checkErrorSpacing(i, "", temp, tokens);
            //checkErrorPattern(i, temp);

            //fixString();
            //correctFixedLine();
        }
    }

    private static void printGroup(Matcher m) {
        System.out.println("Group count: " + m.groupCount());
        int i;
        for (i = 0; i <= m.groupCount(); i++) {
            System.out.println(i + " : " + m.group(i));
        }
    }

    String checkErrorSpacing(int i, String line, String temp, String[] tokens) {
       
        String temp2 = temp;
        Pattern p1 = Pattern.compile("(^\\s+)(.)");
        Matcher m1 = p1.matcher(tokens[i]);
        if (m1.lookingAt()) {
            temp2 = temp2.replaceAll(m1.group(0), m1.group(1) + "^" + m1.group(2));
            System.out.println(line + temp2 + "\t;unexpected space(s) is found athe beginning of the token");
        }
        
        temp2 = temp;
        Pattern p = Pattern.compile(A_STATION + "(\\s+)(\\d+)");
        Matcher m = p.matcher(tokens[i]);
        if (m.find()) {
            temp2 = temp2.replaceAll(m.group(2), m.group(2) + "^");
            System.out.println(line + temp2 + "\t;unexpected space(s) between Station and plant number");
        }
        
        temp2 = temp;
        Pattern p2 = Pattern.compile("(.+)(\\s+)($)");    //space/s at the end of the string or before dash
        Matcher m2 = p2.matcher(tokens[i]);
        if (m2.find()) {
            temp2 = temp2.replaceAll(m2.group(0), m2.group(1) + m2.group(2) + "^");
            System.out.println(line + temp2 + "\t;unexpected space(s) is found athe end of the token");
        }
        //System.out.println("temp: "+ temp);        
        return temp;
    }

    String fixSpacing(String line) {
        String answer;
        do {
            Scanner user_input = new Scanner(System.in);
            System.out.print("\n>>Fix String? (Y/N) ");
            answer = user_input.nextLine();

            if (answer.equalsIgnoreCase("Y")) {

                Pattern p = Pattern.compile(A_STATION + "(\\s+)(\\d+)");
                Matcher m = p.matcher(line);
                if (m.find()) {
                    //printGroup(m);
                    line = line.replaceAll(m.group(0), m.group(1) + m.group(2));
                    System.out.println("processed.");
                    System.out.println("string: " + line + "\n");
                }
                Pattern p1 = Pattern.compile("(^\\s+)(.)");
                Matcher m1 = p1.matcher(line);
                if (m1.find()) {
                    //printGroup(m1);
                    line = line.replaceAll(m1.group(0), m1.group(1));
                    System.out.println("processed.");
                    System.out.println("string: " + line + "\n");
                }
            }
        } while (answer.equalsIgnoreCase("Y") == false);
        return line;
    }
}
