<?php
include("top.txt");


?>

<outercode>

<form action="getFile.php" method="POST" enctype="multipart/form-data">
	<table width="100%";>
		<tr>
			<td width="50%">
				<fieldset>
					<legend style="color:rgb(71, 134, 124);font-weight:bold;">Select List Type</legend>
					<input type="radio" name="group1" value="Breeders Cross Histories" checked> Breeders Cross Histories<br>
					<input type="radio" name="group1" value="Cultivar List" > Cultivar List<br>
					<input type="radio" name="group1" value="Accession"> Accession
				</fieldset><br>
			
				<label for="file">Browse:</label><br>
				<input type="file" name="file" id="file">
				<input type="submit" name="submit" value="Upload List"><br><br>
				
				Location<br><br>
				<select name="location" style="width:100%">
					<option value="location1">location 1</option>
					<option value="location2">location2</option>
					<option value="location3" selected="selected">IRRI HQ</option>
					<option value="location4">location4</option>
				</select>
			</td>
			<td width="50%">
				<fieldset>
					<legend style="color:rgb(71, 134, 124);font-weight:bold;">Map excel Columns to Table Columns</legend>
					<table width="100%">	
						<tr>
							<td width="33.33%">
								Excel Columns<br>
								<textarea name="excel_col" rows="9" cols="30"></textarea>
							</td>
							<td width="33.33%">
								<center>
								<button name="map" type="button">Map</button><br>
								<button name="mapAs" type="button">Map as...</button><br>
								<button name="umap" type="button">Unmap</button><br><br><br>
								<button name="processAll" type="button">Process All</button><br>
								</center>
							</td>
							<td width="33.33%">
								Table Columns<br>
								<textarea name="table_col" rows="9" cols="30"></textarea>
							</td>
						</tr>
					</table>
				
				</fieldset>

			</td>
		</tr>
	</table>
</form>
</outercode>

<outercode>
	<fieldset>
			<legend style="color:rgb(71, 134, 124);font-weight:bold;">Germplasm List</legend>
	</fieldset>
</outercode>

<outercode>
	<table width="100%">
		<tr>
			<td>
			<fieldset>
				<legend style="color:rgb(71, 134, 124);font-weight:bold;">Tree</legend>
				<textarea name="table_col" rows="10" cols="70"></textarea>
			</fieldset>
			</td>
			<td>
			<table width="100%">
				<tr>
					<td>
						<fieldset>
							<legend style="color:rgb(71, 134, 124);font-weight:bold;">Details</legend>
							<textarea name="table_col" rows="4" cols="70"></textarea>
						</fieldset>
					</td>
				</tr><tr>
					<td>
						<fieldset>
							<legend style="color:rgb(71, 134, 124);font-weight:bold;">Events Log</legend>
							<textarea name="table_col" rows="4" cols="70"></textarea>
						</fieldset>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</outercode>


</div>
<!-- END CONTENT -->
<?php
include("bottom.txt");
?>