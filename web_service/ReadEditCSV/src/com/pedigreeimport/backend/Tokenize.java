package com.pedigreeimport.backend;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.generationcp.middleware.exceptions.MiddlewareQueryException;
import org.generationcp.middleware.manager.DatabaseConnectionParameters;
import org.generationcp.middleware.manager.ManagerFactory;
import org.generationcp.middleware.manager.api.GermplasmDataManager;
import org.generationcp.middleware.pojos.Germplasm;
import org.generationcp.middleware.pojos.Name;

/**
 *
 * @author NCarumba
 */
public class Tokenize {

     List<String> listGID = new ArrayList<String>();

    /**
     *
     * @param line 
     * @return  String tokens[] resulting from the split line
     */
    public String[] tokenize(String line) {
        String tokens[] = line.split("-");
        return tokens;
    }

    /**
     *
     * @param tokens 
     * @throws MiddlewareQueryException 
     * @throws IOException 
     */
    public String stringTokens(String tokens[]) throws MiddlewareQueryException, IOException {
    	    FileWriter writer = new FileWriter("C:\\Documents and Settings\\ncarumba\\Desktop\\files\\createdGID.csv");
    	    String s = "", st = "",f1="";
    	    s = "";
    	    for (int i = 0; i < tokens.length;) {
    	    	if (i == 0) {
    	    		s = s + tokens[i];
    	    		f1=s;	//f1 generation
    	    		createGID(s);
    	    	} else {
    	    		s = s + "-" + tokens[i];
    	    		int gid=createGID(s);
    	    		writer.append(""+gid+", \n");
            }
            i++;
            
            //Germplasm germplasm = manager.getGermplasmByGID(new Integer(50533));
            //System.out.println(germplasm);
            	st = st + s + "\n";
        }writer.flush();
	    writer.close();
        //System.out.println(st);
     	return st;
        
    }
    public int createGID(String term) throws MiddlewareQueryException{
    	DatabaseConnectionParameters local = new DatabaseConnectionParameters("localhost", "3306", "local", "root", "");
		ManagerFactory factory = new ManagerFactory(local, null);
		
		GermplasmDataManager manager = factory.getGermplasmDataManager();
		int gid;
		Germplasm germplasm1= new Germplasm();
		germplasm1.setMethodId(105);
		germplasm1.setGnpgs(0);
		germplasm1.setGpid1(1);
		germplasm1.setGpid2(1);
		germplasm1.setUserId(1);
		germplasm1.setLgid(-1);
		germplasm1.setLocationId(104);
		germplasm1.setGdate(0);
		germplasm1.setGrplce(0);
		germplasm1.setMgid(0);
		germplasm1.setReferenceId(1);
		germplasm1.setPreferredAbbreviation("uno");
		germplasm1.setPreferredAbbreviation("uno");
		
		Name name1 =new Name(); 
		name1.setNdate(0);
		name1.setNstat(0);
		name1.setReferenceId(0);
		name1.setUserId(0);
		name1.setLocationId(104);
		name1.setNval(term);
		name1.setTypeId(0);
	
		gid = manager.addGermplasm(germplasm1,name1);
		System.out.print("Germplasm"+gid);
		return gid;
    }
}
