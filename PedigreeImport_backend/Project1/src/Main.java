
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author NCarumba
 */
public class Main {

    private static String line;
    
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        Main m = new Main();
        m.inputString();
        m.checkString();
    }

    private void inputString() {
        Scanner user_input = new Scanner(System.in);

        System.out.print("Enter String:");
        line = user_input.nextLine();
    }

    /**
     * 
     */
    public void checkString() {
        if (line.contains("/")) {
            new BackCross().main(line);
            //new SingleCross().main(line);
        } else {
            Pattern p = Pattern.compile("IR");
            Matcher m = p.matcher(line);

            if (m.lookingAt()) {
                new IRRI().standardIRRI(line);
            }
            Pattern p1 = Pattern.compile("WA");
            Matcher m1 = p1.matcher(line);

            if (m1.lookingAt()) {
                System.out.println("WARDA line");
                new WARDA().standardWARDA(line);
                
            }
        }
    }
}
